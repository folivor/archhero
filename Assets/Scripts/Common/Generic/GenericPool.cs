﻿
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Common.Generic
{
    public class GenericPool<T> where T:MonoBehaviour
    {
        private DiContainer _diContainer;
        private List<T> _objectsInPool;
        private T _prefab;
        private Transform _parent;

        public GenericPool(DiContainer diContainer, T prefab, int preInstance, Transform parent)
        {
            _diContainer = diContainer;
            _prefab = prefab;
            _objectsInPool = new List<T>();
            _parent = parent;
            for (int i = 0; i < preInstance; i++)
            {
                InstanceNewObject();
            } 
        }

        private void InstanceNewObject()
        {
            T newObject;

            if (_diContainer != null)
            {
                newObject = _diContainer.InstantiatePrefabForComponent<T>(_prefab, _parent);
            }
            else
            {
                newObject = GameObject.Instantiate(_prefab, _parent);
            }
            newObject.gameObject.SetActive(false);
            _objectsInPool.Add(newObject);
            ExtendedObjectInitialization(newObject);
        }

        protected virtual void ExtendedObjectInitialization(T newObject)
        {
        }

        public T GetObject()
        {
            if (_objectsInPool.Count == 0)
            {
                InstanceNewObject();
            }

            var returnObject = _objectsInPool[0];
            _objectsInPool.RemoveAt(0);
            return returnObject;
        }

        public void ReturnObject(T objectToPool)
        {
            _objectsInPool.Add(objectToPool);
            objectToPool.gameObject.SetActive(false);
            objectToPool.transform.parent = _parent;
        }
    }
}
