﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using Random = UnityEngine.Random;

namespace Gameplay
{
    public class GameController : MonoBehaviour
    {
        public const string PATH_LEVEL = "Levels/Level";

        [Inject] private readonly DiContainer _diContainer;

        [SerializeField] private PlayerController _playerController;
        [SerializeField] private CameraController _cameraController;
        [SerializeField] private TextMeshProUGUI _timer;
        [SerializeField] private EnemySpawner _enemySpawner;

        private int _coins;

        public int Coins => _coins;

        private LevelController _levelController;

        public LevelController LevelController => _levelController;

        public PlayerController PlayerController => _playerController;

        public EnemySpawner EnemySpawner => _enemySpawner;

        private void OnEnable()
        {
            _enemySpawner.OnAllEnemiesKilled += Win;
        }

        private void OnDisable()
        {
            _enemySpawner.OnAllEnemiesKilled -= Win;
        }

        public void Awake()
        {
            var loadedLevel = Resources.Load<LevelController>(PATH_LEVEL+1);
            _levelController = _diContainer.InstantiatePrefabForComponent<LevelController>(
                loadedLevel.gameObject, Vector3.zero, Quaternion.identity, null);
            
            _playerController = _diContainer.InstantiatePrefabForComponent<PlayerController>(
                _playerController.gameObject, _levelController.SpawnPoint.position, Quaternion.identity, null);
            
            _cameraController.Init(_levelController.Size.x,_playerController.transform);
            
            _enemySpawner.SpawnEnemies(_levelController);
            
            StartGame();
        }

        private void StartGame()
        {
            Time.timeScale = 0f;
            var sequence = DOTween.Sequence();
            for (int i = 0; i < 3; i++)
            {
                var timerCount = 3 - i;
                sequence
                    .AppendCallback(() => _timer.text = timerCount.ToString())
                    .Append(_timer.transform.DOScale(1f, 1f))
                    .Join(_timer.DOFade(0f, 1f))
                    .AppendCallback(() =>
                    {
                        _timer.transform.localScale = Vector3.zero;
                        _timer.alpha = 1f;
                    });
            }

            sequence.OnComplete(() => { Time.timeScale = 1f; });
            sequence.SetUpdate(true);
            sequence.Play();
        }


        public void Win()
        {
            _levelController.EnemiesKilled();
        }

        public void Lose()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

       
    }
}
