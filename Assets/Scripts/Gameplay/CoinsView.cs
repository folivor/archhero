﻿using Gameplay;
using TMPro;
using UnityEngine;
using Zenject;

public class CoinsView : MonoBehaviour
{
    [Inject] private readonly CoinsCounter _coinsCounter;
    
    [SerializeField] private TextMeshProUGUI _text;

    private void Start()
    {
        CoinsViewUpdate();
    }

    private void OnEnable()
    {
        _coinsCounter.OnCoinsUpdate += CoinsViewUpdate;
    }

    private void OnDisable()
    {
        _coinsCounter.OnCoinsUpdate -= CoinsViewUpdate;
    }

    private void CoinsViewUpdate()
    {
        _text.text = _coinsCounter.Coins.ToString();
    }
}
