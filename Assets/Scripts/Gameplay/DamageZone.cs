﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class DamageZone : MonoBehaviour
{
    [HideInInspector] [SerializeField] private Collider _collider;
    [SerializeField] private float _damage;
    
    private void OnTriggerEnter(Collider other)
    {
        var healthComponent = other.GetComponent<HealthComponent>();
        if (healthComponent!= null)
        {
            healthComponent.SetDamage(_damage);
        }
    }

    private void Reset()
    {
        var collider = GetComponent<Collider>();
        collider.isTrigger = true;
        _collider = collider;
    }
}
