﻿using UnityEngine;

namespace Gameplay
{
    public class LevelController : MonoBehaviour
    {
        [SerializeField] private Transform _spawnPoint;
        [SerializeField] private Vector2 _size;
        [SerializeField] private LevelGate _levelGate;
        [SerializeField] private int _enemyCount;
        
        public Transform SpawnPoint => _spawnPoint;

        public Vector2 Size => _size;

        public int EnemyCount => _enemyCount;

        public void EnemiesKilled()
        {
            _levelGate.OpenGate();
        }
    }
}
