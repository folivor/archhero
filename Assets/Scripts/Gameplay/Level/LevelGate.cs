﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gameplay
{
    public class LevelGate : MonoBehaviour
    {
        [SerializeField] private GameObject _gate;
        
        public void OpenGate()
        {
            _gate.SetActive(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<PlayerController>() != null)
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
