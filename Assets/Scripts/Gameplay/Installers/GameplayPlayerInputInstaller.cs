using Gameplay.Input;
using UnityEngine;
using Zenject;

namespace Gameplay.Installers
{
	public class GameplayPlayerInputInstaller : MonoInstaller
	{
		[SerializeField] private GameObject _screenInputParent;
		
		public override void InstallBindings()
		{
#if UNITY_EDITOR
			Container.InstantiateComponent<KeyboardPlayerInput>(_screenInputParent);
			Container.Bind<IPlayerInput>()
				.To<KeyboardPlayerInput>()
				.FromComponentOn(_screenInputParent)
				.AsSingle()
				.NonLazy();
#else
			Container.InstantiateComponent<ScreenPlayerInput>(_screenInputParent);
			Container.Bind<IPlayerInput>()
				.To<ScreenPlayerInput>()
				.FromComponentOn(_screenInputParent)
				.AsSingle()
				.NonLazy();
#endif
		}
	}
}