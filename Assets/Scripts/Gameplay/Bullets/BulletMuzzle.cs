﻿using UnityEngine;
using Zenject;

namespace Gameplay
{
	public class BulletMuzzle : MonoBehaviour
	{
		[Inject] private readonly BulletsPoolController _bulletsPoolController;

		[SerializeField] private BulletID _bulletId;
		[SerializeField] private int _layerBullet;
		[SerializeField] private float _damage;

		public void Shoot()
		{
			var bullet = _bulletsPoolController.GetBullet(_bulletId);
			bullet.transform.SetPositionAndRotation(transform.position,transform.rotation);
			bullet.Damage = _damage;
			bullet.gameObject.layer = _layerBullet;
			bullet.gameObject.SetActive(true);
		}
	}
}