﻿using UnityEngine;

namespace Gameplay
{
    public class ArrowBullet : BaseBullet
    {
        [SerializeField] private float _speed;

        private void Update()
        {
            _rigidbody.MovePosition(transform.position + _speed * Time.deltaTime * transform.forward);
        }

        protected override void TouchObject(Collider other)
        {
            var healthBar = other.GetComponent<HealthComponent>();
            if (healthBar != null)
            {
                healthBar.SetDamage(_damage);
            }

            _isTouched = true;
            gameObject.SetActive(false);
        }
    }
}
