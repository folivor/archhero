﻿using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class BulletsPoolController : MonoBehaviour
    {
        [SerializeField] private List<BaseBullet> _bulletPrefabs;
        [SerializeField] private int _preInstance;

        private List<BulletPool> _bulletPools;

        private void Awake()
        {
            _bulletPools = new List<BulletPool>();

            foreach (var bulletPrefab in _bulletPrefabs)
            {
                var newBulletPool = new BulletPool(null,bulletPrefab,_preInstance,transform);
                _bulletPools.Add(newBulletPool);
            }
        }

        public BaseBullet GetBullet(BulletID id)
        {
            var pool = _bulletPools.Find(a => a.BulletId == id);
            if (pool == null)
                return null;
            return pool.GetObject();
        }
    }
}
