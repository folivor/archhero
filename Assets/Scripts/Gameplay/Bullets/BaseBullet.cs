﻿using System;
using Common.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class BaseBullet : MonoBehaviour
{
    [SerializeField] private BulletID _bulletId;
    [SerializeField] protected Rigidbody _rigidbody;
    private GenericPool<BaseBullet> _pool;
    protected float _damage;
    protected bool _isTouched;

    public BulletID BulletId => _bulletId;
    public GenericPool<BaseBullet> Pool
    {
        set
        {
            if (_pool== null)
                _pool = value;
        }
    }

    public float Damage
    {
        get => _damage;
        set => _damage = value;
    }

    private void OnEnable()
    {
        _isTouched = false;
    }

    private void OnDisable()
    {
        if (_pool!=null)
            _pool.ReturnObject(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.isTrigger || _isTouched)
            return;
        TouchObject(other);
    }

    protected virtual void TouchObject(Collider other)
    {
    }

    private void Reset()
    {
        var rigidBody = GetComponent<Rigidbody>();
        rigidBody.useGravity = false;
        rigidBody.isKinematic = true;
        rigidBody.freezeRotation = true;
        _rigidbody = rigidBody;
        var collider = GetComponent<SphereCollider>();
        collider.isTrigger = true;
    }
}
