﻿using Common.Generic;
using UnityEngine;
using Zenject;

namespace Gameplay
{
	public class BulletPool: GenericPool<BaseBullet>
	{
		private BulletID _bulletId;

		public BulletID BulletId => _bulletId;

		public BulletPool(DiContainer diContainer, BaseBullet prefab, int preInstance, Transform parent) : base(diContainer, prefab, preInstance, parent)
		{
			_bulletId = prefab.BulletId;
		}

		protected override void ExtendedObjectInitialization(BaseBullet newObject)
		{
			newObject.Pool = this;
		}
	}
}