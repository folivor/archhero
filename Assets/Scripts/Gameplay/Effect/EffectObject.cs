﻿using System.Collections;
using Common.Generic;
using UnityEngine;

public class EffectObject : MonoBehaviour
{
    [SerializeField] private EffectID _effectId;
    [SerializeField] private ParticleSystem _particleSystem;

    private GenericPool<EffectObject> _pool;

    public EffectID EffectId => _effectId;

    public GenericPool<EffectObject> Pool
    {
        get => _pool;
        set => _pool = value;
    }

    private void OnDisable()
    {
        if (_pool!= null)
            _pool.ReturnObject(this);
    }

    public void StartEffect()
    {
        _particleSystem.Play();
        StartCoroutine(PlayEffect());
    }

    private IEnumerator PlayEffect()
    {
        while (_particleSystem.isPlaying)
        {
            yield return null;
        }
        gameObject.SetActive(false);
    }
}
