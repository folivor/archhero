﻿using Common.Generic;
using UnityEngine;
using Zenject;

public class EffectPool: GenericPool<EffectObject>
{
	private EffectID _effectId;

	public EffectID EffectId => _effectId;

	public EffectPool(DiContainer diContainer, EffectObject prefab, int preInstance, Transform parent) : base(diContainer, prefab, preInstance, parent)
	{
		_effectId = prefab.EffectId;
	}

	protected override void ExtendedObjectInitialization(EffectObject newObject)
	{
		base.ExtendedObjectInitialization(newObject);
		newObject.Pool = this;
	}
}