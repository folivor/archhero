﻿using System.Collections.Generic;
using UnityEngine;

public class EffectPoolController : MonoBehaviour
{
    [SerializeField] private List<EffectObject> _effectPrefabs;
    [SerializeField] private int _preInstance;

    private List<EffectPool> _bulletPools;

    private void Awake()
    {
        _bulletPools = new List<EffectPool>();

        foreach (var effectObject in _effectPrefabs)
        {
            var newBulletPool = new EffectPool(null,effectObject,_preInstance,transform);
            _bulletPools.Add(newBulletPool);
        }
    }

    public EffectObject GetEffect(EffectID id)
    {
        var pool = _bulletPools.Find(a => a.EffectId == id);
        if (pool == null)
            return null;
        return pool.GetObject();
    }
}