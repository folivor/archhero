using System;
using UnityEngine;

namespace Gameplay.Input
{
	public interface IPlayerInput
	{
		event Action<Vector2> OnAxisUpdate;
	}
}