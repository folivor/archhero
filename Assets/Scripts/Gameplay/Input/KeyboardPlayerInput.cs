﻿using System;
using UnityEngine;

namespace Gameplay.Input
{
	public class KeyboardPlayerInput: MonoBehaviour,IPlayerInput
	{
		public event Action<Vector2> OnAxisUpdate;
		
		private bool _isKeyboardPressed;
		
		private void Update()
		{
			var horizontal = UnityEngine.Input.GetAxis("Horizontal");
			var vertical = UnityEngine.Input.GetAxis("Vertical");
			if (horizontal != 0f || vertical != 0f)
			{
				_isKeyboardPressed = true;
				var axis = new Vector2(horizontal,vertical);
				OnAxisUpdate(axis);
			}
			else
			{
				if (_isKeyboardPressed)
				{
					_isKeyboardPressed = false;
					OnAxisUpdate(Vector2.zero);
				}
			}
		}
	}
}