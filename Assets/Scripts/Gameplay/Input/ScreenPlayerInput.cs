using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Gameplay.Input
{
    public class ScreenPlayerInput : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler, IPlayerInput
    {
        [Inject(Id = "UI")] private readonly CanvasScaler _canvasScaler;
        
        private const float MAX_STICK_RANGE = 136f;
        
        public event Action<Vector2> OnAxisUpdate = x => { };

        private Vector3 _defaultStickPos;
        private Vector2 _startDragPos;

        private float _scaleCoef;
        
        private void Awake()
        {
            UnityEngine.Input.multiTouchEnabled = false;
        }

        private void Start()
        {
            _scaleCoef = _canvasScaler.referenceResolution.x / Screen.width;
        }

        public void OnDrag(PointerEventData eventData)
        {
            var deltaPos = (eventData.position - _startDragPos)/(MAX_STICK_RANGE/_scaleCoef);
            OnAxisUpdate(Vector2.ClampMagnitude(deltaPos,1f));
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _startDragPos = eventData.position;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnAxisUpdate(Vector2.zero);
        }
    }
}