﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Gameplay.Input
{
	public class InputVisualizator:MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
		[Inject] private readonly IPlayerInput _playerInput;
		
		[SerializeField] private RectTransform _baseStick;
		[SerializeField] private RectTransform _stick;
		[SerializeField] private float _maxStickRange;
		
		private Vector2 _defaultStickPos;

		private void Awake()
		{
			_defaultStickPos = _baseStick.anchoredPosition;
		}

		private void OnEnable()
		{
			_playerInput.OnAxisUpdate += StickPositionUpdate;
		}

		private void OnDisable()
		{
			_playerInput.OnAxisUpdate -= StickPositionUpdate;
		}

		private void StickPositionUpdate(Vector2 inputVector)
		{
			_stick.anchoredPosition = inputVector*_maxStickRange;
		}
		
		public void OnPointerDown(PointerEventData eventData)
		{
			_baseStick.position = eventData.position;
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			_baseStick.anchoredPosition = _defaultStickPos;
		}
	}
}