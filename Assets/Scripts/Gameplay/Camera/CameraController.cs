﻿using UnityEngine;

namespace Gameplay
{
    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour
    {
        [HideInInspector] [SerializeField] private Camera _camera;
        [SerializeField] private float _offset;
        
        private Transform _target;

        private bool _isInited;

        public void Init(float widthLevel, Transform player)
        {
            _target = player;
            _camera.orthographicSize = ((widthLevel+1) / _camera.aspect) / 2f; // "+1" - add one block to width of view
            _isInited = true;
        }
        
        private void Update()
        {
            if (!_isInited)
                return;
            var newPos = transform.position;
            newPos.z = _target.position.z + _offset;
            transform.position = newPos;
        }

        private void Reset()
        {
            _camera = GetComponent<Camera>();
        }
    }
}