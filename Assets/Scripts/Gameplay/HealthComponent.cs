﻿using System;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    public event Action OnDeath = () => { };

    [SerializeField] private float _maxHealth = 100f;

    private float _health;

    private bool _isDeath;

    public bool IsDeath => _isDeath;

    private void OnEnable()
    {
        _health = _maxHealth;
    }

    public void SetDamage(float damage)
    {
        if (_isDeath)
            return;
        _health -= damage;
        
        if (_health > 0f) 
            return;
        _isDeath = true;
        OnDeath();
        _health = 0f;
    }
}
