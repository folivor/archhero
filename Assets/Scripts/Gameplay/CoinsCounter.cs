﻿using System;
using UnityEngine;

namespace Gameplay
{
	public class CoinsCounter : MonoBehaviour
	{
		public event Action OnCoinsUpdate = () => { };
		
		private int _coins;

		public int Coins => _coins;
		
		public void AddCoins(int coins)
		{
			_coins += coins;
			OnCoinsUpdate();
		}
	}
}