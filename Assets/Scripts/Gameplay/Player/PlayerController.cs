﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay.Input;
using UnityEngine;
using Zenject;

namespace Gameplay
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        [Inject] private readonly IPlayerInput _screenPlayerInput;
        [Inject] private readonly BulletsPoolController _bulletsPoolController;
        [Inject] private readonly GameController _gameController;
        
        [HideInInspector] [SerializeField] private CharacterController _characterController;
        [SerializeField] private Animator _animator;
        [SerializeField] private float _speed;
        [SerializeField] private float _atkSpeed;
        [SerializeField] private HealthComponent _healthComponent;
        [SerializeField] private List<BulletMuzzle> _bulletMuzzles;
        
        private bool _isMoving;
        private bool _isCanShoot;
        private Vector3 _directionInput;
        private readonly int _runForward = Animator.StringToHash("Run Forward");
        private readonly int _attackTrigger = Animator.StringToHash("Attack 01");
        private bool _isDeath;
        
        private void OnEnable()
        {
            _healthComponent.OnDeath += Death;
            _screenPlayerInput.OnAxisUpdate += UpdateDirectionInput;
            _isCanShoot = true;
        }

        private void OnDisable()
        {
            _healthComponent.OnDeath -= Death;
            _screenPlayerInput.OnAxisUpdate -= UpdateDirectionInput;
        }

        private void UpdateDirectionInput(Vector2 directionInput)
        {
            _directionInput = new Vector3(directionInput.x,0f,directionInput.y);
        }

        private void Update()
        {
            if (_isDeath)
                return;
            if (Math.Abs(_directionInput.x) > 0.01f || Math.Abs(_directionInput.z) > 0.01f)
            {
                _characterController.Move(_speed * Time.deltaTime * _directionInput);
                transform.forward = _directionInput;
                if (_isMoving) return;
                _isMoving = true;
                _animator.SetBool(_runForward, _isMoving);
                return;
            }
            
            if (_isMoving)
            {
                _isMoving = false;
                _animator.SetBool(_runForward, _isMoving);
            }
            
            TryShoot();
        }

        private void TryShoot()
        {
            if (!_isCanShoot || !Targeting()) return;
            _isCanShoot = false;
            _animator.SetTrigger(_attackTrigger);
            _bulletMuzzles.ForEach(a=>a.Shoot());
            StartCoroutine(AttackRestore());
        }

        private bool Targeting()
        {
            var enemies = _gameController.EnemySpawner.EnemiesInGame;
            if (enemies.Count == 0)
                return false;
            var targetEnemy = enemies[0];
            var currentDistance = (targetEnemy.transform.position - transform.position).sqrMagnitude;
            if (enemies.Count > 1)
            {
                for (var i = 1; i < enemies.Count; i++)
                {
                    var enemy = enemies[i];
                    var distance = (enemy.transform.position - transform.position).sqrMagnitude;
                    if (currentDistance > distance)
                    {
                        targetEnemy = enemy;
                        currentDistance = distance;
                    }
                }
            }

            var pos = targetEnemy.transform.position;
            pos.y = 0f;
            transform.forward = pos - transform.position;
            return true;
        }

        private IEnumerator AttackRestore()
        {
            yield return new WaitForSeconds(_atkSpeed);
            _isCanShoot = true;
        }

        private void Death()
        {
            _gameController.Lose();
        }

        private void Reset()
        {
            _characterController = GetComponent<CharacterController>();
        }
    }
}
