﻿using System.Collections;
using UnityEngine;

namespace Gameplay
{
    public class FlyingEnemy : BaseEnemy
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _speedRush;
        [SerializeField] private float _rushDistance;
        [SerializeField] private float _restoreRush;
        
        private bool _canRush = false;
        private bool _inRush = false;

        protected override void OnEnable()
        {
            base.OnEnable();
            _canRush = false;
            _inRush = false;
            StartCoroutine(RestoreRush());
        }

        protected override void UpdateEnemy()
        {
            base.UpdateEnemy();
            if (_inRush)
            return;
            if (_canRush)
            {
                StartCoroutine(StartRush());
            }
            else
            {
                MoveToPlayer();
            }

        }

        private void MoveToPlayer()
        {
            transform.forward = _gameController.PlayerController.transform.position - transform.position;
            _characterController.Move(_speed * Time.deltaTime * transform.forward);
        }

        private IEnumerator StartRush()
        {
            _inRush = true;
            _canRush = false;
            var time = _rushDistance / _speedRush;
            while (time>0f)
            {
                time -= Time.deltaTime;
                _characterController.Move(_speedRush * Time.deltaTime * transform.forward);
                yield return null;
            }

            _inRush = false;
            StartCoroutine(RestoreRush());
        }

        private IEnumerator RestoreRush()
        {
            _canRush = false;
            yield return new WaitForSeconds(_restoreRush);
            _canRush = true;
        }
        
        protected override IEnumerator DeathAnimation()
        {
            var effect = _effectPoolController.GetEffect(EffectID.explosion);
            effect.transform.position = transform.position;
            effect.gameObject.SetActive(true);
            effect.StartEffect();
            gameObject.SetActive(false);
            yield break;
        }
    }
}
