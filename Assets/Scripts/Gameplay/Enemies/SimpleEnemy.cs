﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class SimpleEnemy : BaseEnemy
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private float _speed;
        [SerializeField] private float _atkSpeed;
        [SerializeField] private float _changePositionRange;
        [SerializeField] private List<BulletMuzzle> _bulletMuzzles;

        private bool _isCanShoot;
        private bool _isMoving;
        private readonly int _runForward = Animator.StringToHash("Run Forward");
        private readonly int _attack = Animator.StringToHash("Attack 01");

        protected override void OnEnable()
        {
            base.OnEnable();
            _isMoving = false;
            _isCanShoot = false;
            StartCoroutine(AttackRestore());
        }

        protected override void UpdateEnemy()
        {
            base.UpdateEnemy();
            if (_isMoving)
                return;
            TryShoot();
        }

        private void TryShoot()
        {
            var directionShoot = _gameController.PlayerController.transform.position;
            directionShoot -= transform.position;
            transform.forward = directionShoot;
            if (!_isCanShoot)
                return;
            _isCanShoot = false;
            _bulletMuzzles.ForEach(a=>a.Shoot());
            _animator.SetTrigger(_attack);
            StartCoroutine(AttackRestore());
            StartCoroutine(MovingToNewPosition(0.5f));//just random value for delay after shooting
        }

        private IEnumerator MovingToNewPosition(float delay)
        {
            _isMoving = true;
            var newDirection = new Vector3(Random.Range(-1f,1f),0,Random.Range(-1f,1f)).normalized;
            yield return new WaitForSeconds(delay);
            _animator.SetBool("Run Forward",true);
            transform.forward = newDirection;
            var time = _changePositionRange / _speed;
            while (time>0f)
            {
                time -= Time.deltaTime;
                _characterController.Move(_speed * Time.deltaTime * newDirection);
                yield return null;
            }
            _animator.SetBool(_runForward,false);
            _isMoving = false;
        }

        private IEnumerator AttackRestore()
        {
            yield return new WaitForSeconds(_atkSpeed);
            _isCanShoot = true;
        }

        protected override IEnumerator DeathAnimation()
        {
            var effect = _effectPoolController.GetEffect(EffectID.explosion);
            effect.transform.position = transform.position;
            effect.gameObject.SetActive(true);
            effect.StartEffect();
            gameObject.SetActive(false);
            yield break;
        }
    }
}