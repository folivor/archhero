﻿using System;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Gameplay
{
    
    [RequireComponent(typeof(CharacterController))]
    public class BaseEnemy : MonoBehaviour
    {
        [Inject] protected readonly EffectPoolController _effectPoolController;
        [Inject] protected readonly GameController _gameController;
        [Inject] protected readonly CoinsCounter _coinsCounter;
        
        [HideInInspector] [SerializeField] protected CharacterController _characterController;
        [SerializeField] private HealthComponent _healthComponent;
        [SerializeField] private int _coins;
        
        private bool _isDeath;

        public bool IsDeath => _isDeath;

        protected virtual void OnEnable()
        {
            _isDeath = false;
            _healthComponent.OnDeath += Death;
        }

        protected void OnDisable()
        {
            _healthComponent.OnDeath -= Death;
        }

        // Update is called once per frame
        private void Update()
        {
            if (_isDeath)
                return;
            UpdateEnemy();
        }

        protected virtual void UpdateEnemy()
        {
        }

        private void Death()
        {
            if (_isDeath)
                return;
            _isDeath = true;
            _gameController.EnemySpawner.EnemyKilled(this);
            _coinsCounter.AddCoins(_coins);
            StartCoroutine(DeathAnimation());
        }

        protected virtual IEnumerator DeathAnimation()
        {
            gameObject.SetActive(false);
            yield break;
        }

        private void Reset()
        {
            _characterController = GetComponent<CharacterController>();
        }
    }
}
