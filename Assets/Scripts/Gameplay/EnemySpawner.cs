﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Gameplay
{
	public class EnemySpawner : MonoBehaviour
	{
		public event Action OnAllEnemiesKilled = () => { };

		[Inject] private DiContainer _diContainer;
		[SerializeField] private List<BaseEnemy> _enemies;

		private List<BaseEnemy> _enemiesInGame;

		public List<BaseEnemy> EnemiesInGame => _enemiesInGame;
		
		public void SpawnEnemies(LevelController levelController)
		{
			_enemiesInGame = new List<BaseEnemy>();
			for (int i = 0; i < levelController.EnemyCount; i++)
			{
				var xRange = levelController.Size.x / 2f;
				var yRange = (levelController.Size.y / 3f) * 1f;
				var randomPos =
					new Vector3(Random.Range(-xRange, xRange), 0f, Random.Range(yRange, levelController.Size.y)) +
					levelController.transform.position;
				var newEnemy = _diContainer.InstantiatePrefabForComponent<BaseEnemy>(_enemies[Random.Range(0, _enemies.Count)],
					randomPos, Quaternion.identity, levelController.transform);
				_enemiesInGame.Add(newEnemy);
			}
		}
		
		public void EnemyKilled(BaseEnemy enemy)
		{
			_enemiesInGame.Remove(enemy);
			if (_enemiesInGame.Count == 0)
			{
				OnAllEnemiesKilled();
			}
		}
	}
}